# Cassette

A subjective, directory-based MP3 meta-data organization tool.

## How do I install this?

1. Clone into the repository
2. Build it directly with `go build` or install it into your $GOPATH/bin folder with `go install`
3. Run the binary as described below

## How does it work?

1. Sort all your music into a recursive directory structure
    - It doesn't matter what directory means what, just that it's consistent
    - Ex: ~/Music/%GENRE%/%ARTIST%/%ALBUM%/%SONG%.mp3
2. Write a .cassrc file (either in your home directory or one that you specify)
    - A .cassrc file is a YAML file which contains the data needed to sort files
    - The only thing in the YAML file at this point is the 'pathrep' option, which is in the format of:
        - PROP_1/PROP_2/PROP_3/PROP_4, where PROP_[1-4] are all valid ID3 tags in the supported list
    - For the above file structure, a valid .cassrc file is:
        - pathrep: genre/artist/album/title
3. Run the binary on the folders you want to organize

## What are the supported ID3 tags?

The currently supported tags are:

- Title
- Artist
- Album
- Year
- Genre

I've chosen these tags for this project because they're the easiest to implement, the most relevant for my purposes and fit best in a file hierarchy without causing major issue. I may choose to implement more of these if the need arises, but these are the final list for now.

## Why does this need to exist?

I dunno. I wanted to make my music easier to sort. I already sort them through my file hierarchy and figured there's no good reason to apply the ID3 tags individually anyways.

## Why doesn't this support X feature?

I dunno. I wrote this for 90% my own purposes and don't want to add anything that would complicate it past the point that I'm not able to easily use it. Sorry.

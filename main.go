package main

import (
    id3 "github.com/mikkyang/id3-go"
    yaml "gopkg.in/yaml.v2"
    "os"
    "os/user"
    "fmt"
    "io/ioutil"
    "flag"
    "path/filepath"
    "strings"
)

// TODO:
// multithreading - id3 library breaks this?
// Album art?

// the possible catagories
var catagories = [...]string{"title", "artist", "album", "year", "genre"}

// a possible configuration
type configuration struct {
    PathRep string
    Parts []string
}

// the user's prefered configuration
var conf configuration

// Checks if a file ends in .mp3
func isMP3(fName string) (bool) {
    return filepath.Ext(fName) == ".mp3"
}

func main() {
    // setup/capture flags
    flag.Usage = func() {
        fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s: [options] directory\n\t", os.Args[0])
        flag.PrintDefaults()
    }
    var confPath *string
    if usr, err := user.Current(); err != nil {
        fmt.Print("Error found in getting user home")
        os.Exit(1)
    } else {
        confPath = flag.String("conf", usr.HomeDir + "/.config/cassrc", "The location of your cassrc config file")
    }
    flag.Parse()

    // ensure start directory is given
    if len(flag.Args()) == 0 {
        fmt.Println("Must input a directory to catalog")
        os.Exit(1)
    }

    // evaluate any possible symlinks
    nonLinkSDir, err := filepath.EvalSymlinks(flag.Args()[0])
    if err != nil {
        fmt.Printf("Found error when trying to eval symlinks: %s\n", err)
        os.Exit(1)
    }

    // ensure start directory exists and is a directory
    startDir, err := os.Open(nonLinkSDir)
    if err != nil {
        fmt.Printf("Found error when trying to open \"%s\": \"%s\"\n", flag.Args()[0], err)
        os.Exit(1)
    } else if finfo, err := startDir.Stat(); err != nil {
        fmt.Printf("Could not collect file header on \"%s\"\n", flag.Args()[0])
        os.Exit(1)
    } else if !finfo.IsDir() {
        fmt.Printf("\"%s\" was found not to be a directory\n", flag.Args()[0])
        os.Exit(1)
    }

    // read config file and ensure it exists
    confText, err := ioutil.ReadFile(*confPath)
    if err != nil {
        fmt.Printf("Error opening config file \"%s\": %s\n", *confPath, err)
        os.Exit(1)
    }

    // parse config file content
    err = yaml.Unmarshal(confText, &conf)
    if err != nil {
        fmt.Printf("Could not parse config file: \"%s\"\n", *confPath)
        os.Exit(1)
    }

    // lowercase split into parts and reverse the path representation
    conf.PathRep = strings.ToLower(conf.PathRep)
    conf.Parts = strings.Split(conf.PathRep, "/")

    // ensure that each of the parts is a valid catagory
    for _, part := range conf.Parts {
        for _, attr := range catagories {
            if attr != part {
                fmt.Printf("Could not find id3 catagory \"%s\"\n", part)
                os.Exit(1)
            }
        }
    }

    // walk the start directory and process each file
    err = filepath.Walk(startDir.Name(), func(path string, info os.FileInfo, err error) error {
        // check for error
        if err != nil {
            fmt.Printf("Found error at file %v: %v\n", err)
            return err
        }

        // ignore if directory or non-MP3
        if info.IsDir() || !isMP3(path) {return nil}

        // This library is not thread-safe, apparently, so single execution
        handleMP3(path, startDir.Name())

        return nil
    })

    // report errors found in tree-walking
    if err != nil {
        fmt.Printf("Found error while perusing files: %s\n", err)
    }
}

func handleMP3(path, startLoc string) {
    // remove initial startDir name and extention
    relPath := strings.TrimPrefix(path, startLoc)
    relPath = strings.TrimPrefix(relPath, "/")
    relPath = strings.TrimSuffix(relPath, ".mp3")

    // split path into attributes
    attrs := strings.Split(relPath, "/")

    // open an mp3file and ensure it exists
    mp3File, err := id3.Open(path)
    if (err != nil) {
        fmt.Printf("Found error %s when attempting to open %s", err, path)
        os.Exit(1)
    }

    // for each of the attributes
    for i := 0; i < len(attrs); i++ {
        // get the ith last attribute
        attr := attrs[len(attrs) - (i + 1)]

        // get the ith last configuraiton part, if it exists
        if len(conf.Parts) - (1 + i) < 0 {
            break
        }
        part := conf.Parts[len(conf.Parts) - (i + 1)]

        switch part {
            case "title":
                mp3File.SetTitle(attr)
            case "artist":
                mp3File.SetArtist(attr)
            case "genre":
                mp3File.SetGenre(attr)
            case "album":
                mp3File.SetAlbum(attr)
            case "year":
                mp3File.SetYear(attr)
        }
    }

    mp3File.Close()
}
